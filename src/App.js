import React, { Component } from 'react';
import logo from './logo.jpg';
import firebase,{auth } from './firebase';
 
class App extends Component {
 
  constructor(){
     super();
     this.state = {
        resname:[],
        items:[],
        item_id:'',
        title:'',
        description:'',
        email:'',//add email state
        password:'',//add password state
        isLogin:true //add isLogin state
     }
 
     this.handleChange = this.handleChange.bind(this)
     this.handleUpdate = this.handleUpdate.bind(this)
     this.handleSubmit = this.handleSubmit.bind(this)
 
     this.login = this.login.bind(this)
     this.logout = this.logout.bind(this)
 
  }
 
  componentDidMount(){
 
      auth.onAuthStateChanged((user) => {
          if(user){
            this.setState({user})
          }
      })
 
      const itemsRef = firebase.database().ref('items');
      itemsRef.on('value',(snapshot) => {
          let items = snapshot.val();
          let newState = [];
          for(let item in items){
            newState.push({
                item_id:item,
                title:items[item].title,
                description:items[item].description
            })
          }
          this.setState({
            items:newState
          })
      })
      const resnameRef = firebase.database().ref('resname');
      resnameRef.on('value',(snapshot) => {
          let resname = snapshot.val();
          let newState = [];
          for(let item in resname){
            newState.push({
                item_id:item,
                title:resname[item].title,
                description:resname[item].description
            })
          }
          this.setState({
            resname:newState
          })
      })
      
  }
  
  handleChange(e){
    this.setState({
      [e.target.name]: e.target.value
    })
  }
 
 
  handleSubmit(e){
    e.preventDefault();
 
    if(this.state.item_id !== ''){
      return this.updateItem();
    }
 
    const itemsRef = firebase.database().ref('resname')
    const item = {
       title : this.state.title,
       description : this.state.description
    }
    itemsRef.push(item)
    this.setState({
       item_id:'',
       title:'',
       description:''
    })
 }
 
 
  handleUpdate = (item_id = null , title = null , description = null) => {
    this.setState({item_id,title,description})
  }
 
  updateItem(){
 
      var obj = { title:this.state.title,description:this.state.description }
 
      const itemsRef = firebase.database().ref('/items')
 
      itemsRef.child(this.state.item_id).update(obj);
 
      this.setState({
        item_id:'',
        title:'',
        description:''
      })
 
  }
 
  removeItem(itemId){
    const itemsRef = firebase.database().ref('/items');
    itemsRef.child(itemId).remove();
 }
 
 
 login = () =>{
 
  firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(() => {
   
   this.setState({ isLogin : true });
 
  }).catch(function(error) {
     var errorCode = error.code;
     var errorMessage = error.message;
     if (errorCode === 'auth/wrong-password') {
       alert('Wrong password.');
     } else {
       alert(errorMessage);
     }
     console.log(error);
   });
}
 
logout(){
  auth.signOut().then(() => {
     this.setState({isLogin:false})
  }) 
}
 
 
loginForm(){
  return(
    <div>
         <main role="main" className="container" style={{marginTop:80}}>
         <div className="row">
           <div className="col-4"></div>
             <div className="col-4">
                 <form>
                   <div class="form-group">
                     <label for="exampleInputEmail1">Email address</label>
                     <input type="email" class="form-control" name="email"  onChange={this.handleChange} value={this.state.email} placeholder="Enter email"/>
                     <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                   </div>
                   <div class="form-group">
                     <label for="exampleInputPassword1">Password</label>
                     <input type="password" class="form-control" name="password" onChange={this.handleChange} value={this.state.password} placeholder="Password"/>
                   </div>
                   <button class="btn btn-primary" onClick={() => this.login()}>Log In</button>
                 </form>
             </div>
           </div>
         </main>
     </div>
  )
}
 
 
  render() {
 
 
  if(!this.state.isLogin){
      return this.loginForm()
   }
 
 
    return (
      <div className="app">
          <nav class="navbar navbar-light bg-warning">
          <img src={logo} width="80px" height = "55px" alt="My logo" />
            <span class="navbar-brand mb-0 h1">Givemefood</span>
            {
                this.state.isLogin ? <button className="btn btn-danger"  onClick={() => this.logout()} >Logout</button> : null 
            }
          </nav>
          <div className="container" style={{marginTop:70}}>
          <form  onSubmit={this.handleSubmit}>
            <div className="row">
                <div className="col-8">
                  <div className="form-row">
                    
                  </div>
                </div>
            </div>
          </form>
        <hr/>
              <table className="table table-sm table-bordered">
                    <tr className="thead-dark">
                      <th width="20%">UserName</th>
                      <th width="70%">Description</th>
               
                      <th width="5%">Delete</th>
                    </tr>
                    {
                        this.state.items.map((item) => {
                          return (
                              <tr>
                                <td>{item.title}</td>
                                <td>{item.description}</td>
                   
                                <td><button className="btn btn-danger btn-sm" onClick={() => this.removeItem(item.item_id)}>Delete</button></td>
                              </tr>
                          )
                        })
                    }
                </table>
                <table className="table table-sm table-bordered">
                    <tr className="thead-dark">
                      <th width="20%">Restaurant name</th>
                      <th width="70%">Description</th>
                      <th width="5%">Delete</th>
                    </tr>
                    {
                        this.state.resname.map((item) => {
                          return (
                              <tr>
                                <td>{item.title}</td>
                                <td>{item.description}</td>
                          
                                <td><button className="btn btn-danger btn-sm" onClick={() => this.removeItem(item.item_id)}>Delete</button></td>
                              </tr>
                          )
                        })
                    }
                </table>
          </div>
          
      </div>
    );
  }
}
 
export default App;