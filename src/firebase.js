import * as firebase from 'firebase';
 
//insert config from firebase
const config = {
    apiKey: "AIzaSyAL5cfmIcCllTSvf5rvgdkoCX1SHVoWauQ",
    authDomain: "givemefood-89ef5.firebaseapp.com",
    databaseURL: "https://givemefood-89ef5.firebaseio.com",
    projectId: "givemefood-89ef5",
    storageBucket: "givemefood-89ef5.appspot.com",
    messagingSenderId: "520364301210"
};
firebase.initializeApp(config);
 
//add code firebase.auth
export const auth = firebase.auth();
export default firebase;